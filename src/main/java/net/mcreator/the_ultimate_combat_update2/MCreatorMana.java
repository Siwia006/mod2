package net.mcreator.the_ultimate_combat_update2;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.ItemStack;
import net.minecraft.item.Item;
import net.minecraft.block.BlockState;

@Elementsthe_ultimate_combat_update2.ModElement.Tag
public class MCreatorMana extends Elementsthe_ultimate_combat_update2.ModElement {
	@ObjectHolder("the_ultimate_combat_update2:mana")
	public static final Item block = null;

	public MCreatorMana(Elementsthe_ultimate_combat_update2 instance) {
		super(instance, 2);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new ItemCustom());
	}

	public static class ItemCustom extends Item {
		public ItemCustom() {
			super(new Item.Properties().group(null).maxStackSize(1));
			setRegistryName("mana");
		}

		@Override
		public int getItemEnchantability() {
			return 0;
		}

		@Override
		public int getUseDuration(ItemStack itemstack) {
			return 0;
		}

		@Override
		public float getDestroySpeed(ItemStack par1ItemStack, BlockState par2Block) {
			return 1F;
		}
	}
}
