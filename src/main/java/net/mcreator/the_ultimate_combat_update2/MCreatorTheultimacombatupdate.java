package net.mcreator.the_ultimate_combat_update2;

import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemGroup;

@Elementsthe_ultimate_combat_update2.ModElement.Tag
public class MCreatorTheultimacombatupdate extends Elementsthe_ultimate_combat_update2.ModElement {
	public MCreatorTheultimacombatupdate(Elementsthe_ultimate_combat_update2 instance) {
		super(instance, 3);
	}

	@Override
	public void initElements() {
		tab = new ItemGroup("tabtheultimacombatupdate") {
			@OnlyIn(Dist.CLIENT)
			@Override
			public ItemStack createIcon() {
				return new ItemStack(MCreatorExpelliarmus.block, (int) (1));
			}

			@OnlyIn(Dist.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
	}
	public static ItemGroup tab;
}
