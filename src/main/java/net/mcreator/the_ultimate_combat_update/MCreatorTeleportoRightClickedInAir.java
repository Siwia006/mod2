package net.mcreator.the_ultimate_combat_update;

import net.minecraft.util.math.RayTraceContext;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

@Elementsthe_ultimate_combat_update.ModElement.Tag
public class MCreatorTeleportoRightClickedInAir extends Elementsthe_ultimate_combat_update.ModElement {
	public MCreatorTeleportoRightClickedInAir(Elementsthe_ultimate_combat_update instance) {
		super(instance, 22);
	}

	public static void executeProcedure(java.util.HashMap<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure MCreatorTeleportoRightClickedInAir!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if (entity instanceof LivingEntity)
			((LivingEntity) entity).setPositionAndUpdate(
					(entity.world.rayTraceBlocks(
							new RayTraceContext(entity.getEyePosition(1f), entity.getEyePosition(1f).add(entity.getLook(1f).x * 100,
									entity.getLook(1f).y * 100, entity.getLook(1f).z * 100), RayTraceContext.BlockMode.OUTLINE,
									RayTraceContext.FluidMode.NONE, entity)).getPos().getX()),
					(entity.world.rayTraceBlocks(
							new RayTraceContext(entity.getEyePosition(1f), entity.getEyePosition(1f).add(entity.getLook(1f).x * 100,
									entity.getLook(1f).y * 100, entity.getLook(1f).z * 100), RayTraceContext.BlockMode.OUTLINE,
									RayTraceContext.FluidMode.NONE, entity)).getPos().getY()),
					(entity.world.rayTraceBlocks(
							new RayTraceContext(entity.getEyePosition(1f), entity.getEyePosition(1f).add(entity.getLook(1f).x * 100,
									entity.getLook(1f).y * 100, entity.getLook(1f).z * 100), RayTraceContext.BlockMode.OUTLINE,
									RayTraceContext.FluidMode.NONE, entity)).getPos().getZ()));
	}
}
